
Page({
  /**
   * 页面的初始数据
   */
  data: {
    scrollheight:0,
    height: 0,
    // 产品分类大列表
    
    typeData: [
      {id:1, text:'教育', judgeAct:"activecat"},
      {id:2, text:'服务', judgeAct:""},
      {id:3, text:'技术', judgeAct:""},
      {id:4, text:'自媒体', judgeAct:""},
      {id:5, text:'互联网', judgeAct:""},
      {id:6, text:'运营', judgeAct:""},
      {id:7, text:'设计', judgeAct:""},
      {id:8, text:'金融', judgeAct:""},
      {id:9, text:'产品', judgeAct:""},
      {id:10, text:'翻译', judgeAct:""},
      {id:11, text:'其他', judgeAct:""},
      {id:12, text:'到底啦', judgeAct:""}
    ],
    // 小学
    tags1:[
      { message:'语文'  },
      { message:'数学'  },
      { message:'英语'  }
    ],
    // 初中
    tags2:[
      { message:'语文'  },
      { message:'数学'  },
      { message:'英语'  }
    ],
    tags3:[
      { message:'物理'  },
      { message:'化学'  },
      { message:'生物'  }
    ],
    tags4:[
      { message:'历史'  },
      { message:'地理'  },
      { message:'政治'  }
    ],
    // 高中
    tags5:[
      { message:'语文'  },
      { message:'数学'  },
      { message:'英语'  }
    ],
    tags6:[
      { message:'物理'  },
      { message:'化学'  },
      { message:'生物'  }
    ],
    tags7:[
      { message:'历史'  },
      { message:'地理'  },
      { message:'政治'  }
    ],

    // 其他
    tags8:[
      { message:'钢琴'  },
      { message:'竖笛'  },
      { message:'古筝'  }
    ],
    tags9:[
      { message:'街舞'  },
      { message:'唱歌'  },
      { message:'架子鼓'  }
    ],
    tags10:[
      { message:'RAP'  },
      { message:'小提琴'  },
      { message:'二胡'  }
    ],

  },



  handleActive: function(e) {
    var dd = this.data.typeData
    var id = e.currentTarget.id
    // console.log(e.currentTarget.id);
    // console.log(e);

    for(var i=0;i<dd.length;i++)
    {
      if(dd[i].id == id)
        {
          dd[i].judgeAct = "activecat";
          // console.log(dd[i].id + "变active");
        }
      else
        {
          dd[i].judgeAct = "";
          // console.log(dd[i].id + "变null");
        }
    }
    this.setData({
      typeData: dd
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})