
var app=getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    show:-1,
    itemShow:[0,0,0,0],
    searchItems:['筛选'],
    selectItems:[
      ['全部筛选','距离最近','最新发布','浏览最多','评价最好'],
    ],
    brandList:[
      { message:'1'  },
      { message:'2'  },
      { message:'3'  },
      { message:'4'  },
      { message:'5'  },
      { message:'6'  }
    ],
    score:4.2,
    pageIndex:1,
    pageIndex:1,
    name:'',
    jobname:'',
    yaoqiu:'',
    didian:'',
    time:'',
    money:'',
    email:'',
    sendtime:'',
    tiaojian:'',
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     let that =this;
     const db=wx.cloud.database();
     db.collection("qiuzhi").orderBy('sendtime','desc').get({
       success:res=>{
         console.log("求职数据获取成功")
         that.setData({
           content:res.data
         })
       },
       fali:res=>{
         console.log("获取所有数据失败",res);
       }
     })

    
  },

  
  
  goToPostNeed:function() {
    wx.navigateTo({
      url: "/pages/postNeed/postNeed",
    });
    console.log("==== go to postPostNeed ");
  },
  goToQiuZhiTags:function()  {
    wx.navigateTo({
      url: "/pages/qiuZhiTags/qiuZhiTags",
    });
    console.log("==== go to qiuZhiTags ");
  },
  goToQiuZhiContent:function(e){
    
    console.log("==== go to qiuZhiContent ");
    console.log(e.currentTarget.dataset.idx);
    wx.navigateTo({
      url: "/pages/qiuZhiContent/qiuZhiContent?idx=" + e.currentTarget.dataset.idx,
    });
  },

    
    

   
    onPullDownRefresh: function(){
      var that=this;
      that.setData({
        currentTab:0
      })
      this.onLoad();
      console.log("下拉刷新");
    }
  
})