
var app=getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    
    show:-1,
    itemShow:[0,0,0,0],
    searchItems:['筛选'],
    selectItems:[
      ['全部筛选','距离最近','最新发布','浏览最多','评价最好'],
    ],
    tags:[
      { message:'每天两小时'  },
      { message:'每周至少三次'  },
      { message:'本科生'  }
    ],
    brandList:[
      { message:'1'  },
      { message:'2'  },
      { message:'3'  },
      { message:'4'  },
      { message:'5'  },
      { message:'6'  }
    ],
    score:4.2,
    pageIndex:1,
    name:'',
    jobname:'',
    yaoqiu:'',
    didian:'',
    time:'',
    money:'',
    email:'',
    sendtime:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that =this;
    const db=wx.cloud.database();
      db.collection("zhaopin").orderBy('sendtime','desc').get({
        success:res=>{
          console.log("招聘获取数据成功")
          that.setData({
            content:res.data
          })
        },
        fali:res=>{
          console.log("招聘获取数据失败",res);
        }

      })
     //获取用户openid
    wx.cloud.callFunction({
      name: 'getopenID',
      data: {},
      success: res =>{
        app.globalData.openid =res.result.openid;
        console.log("11");
        console.log(app.globalData.openid);
      },
      fail: err =>{
        console.error("云函数调用失败", err);
      },
      
    })
    
      wx.request({
        url: app.globalData.urlBase +'/merchants',
        success:res=>{
          this.setData({"brandList":res.data});
        }
      })
      console.log(app.globalData.openid);



  },

  onPullDownRefresh: function(){
    var that=this;
    that.setData({
      currentTab:0
    })
    this.onLoad();
    console.log("下拉刷新");
  },

  goToPostRelease:function() {
    wx.navigateTo({
      url: "/pages/postRelease/postRelease",
    });
    console.log("==== go to postRealease ");
  },
  goToPostLookPin:function()  {
    wx.navigateTo({
      url: "/pages/lookPin/lookPin",
    });
    console.log("==== go to lookPin ");
  },
  goToJobClass:function() {
    wx.navigateTo({
      url: "/pages/jobClass/jobClass",
    });
    console.log("==== go to jobClass ");
  },


  goToZhaoPinContent:function(e){
    console.log("==== go to zhaoPinContent ");
    console.log(e.currentTarget.dataset.idx)
    wx.navigateTo({
      url: "/pages/zhaoPinContent/zhaoPinContent?idx="+e.currentTarget.dataset.idx,
    });
    
  },


  onTypeTap:function(e){
    var num = e.currentTarget.dataset.num;
    if(num==this.data.show){
      num=-1;
    }
    this.setData({show:num});
  },
  onSelectItemTap:function(e){  
    var column = e.currentTarget.dataset.column;
    var row = e.currentTarget.dataset.row;
    var itemShow=this.data.itemShow;
    itemShow[column]=row;
    var searchItems=this.data.searchItems;
    searchItems[column]=this.data.selectItems[column][row];
    this.setData(
        {
          itemShow:itemShow,
          show:-1,
          searchItems:searchItems
        }
    );
    this.checkSearchItemName();

    var shopType=this.data.itemShow[0];
    var building=this.data.itemShow[1];
    var floor=this.data.selectItems[2][this.data.itemShow[2]];
    if(floor=='全部'){
      floor='';
    }
    var avgPrice=this.data.itemShow[3];
    var minPrice=-1;
    var maxPrice=-1;
    if(avgPrice==1){
      maxPrice=100;
    }else if(avgPrice==2){
      minPrice=100;
      maxPrice=200;
    }else if(avgPrice==3){
      minPrice = 300;
      maxPrice = 500;
    }else if(avgPrice==4){
      minPrice=500;
    }
    //console.log(shopType+","+building+','+floor+','+minPrice+","+maxPrice);
    wx.request({
      url: app.globalData.urlBase + '/merchants',
      data: {
        shopType:shopType,
        building:building,
        floor:floor,
        priceMin:minPrice,
        priceMax:maxPrice
      },
      success: res => {
        console.log(res.data);
        this.setData({ "brandList": res.data });
      }
    })
  },
  onBrandCardTap:function(e){
    var id=e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/shop/shop?id='+id,
    })
  },
  handleSearch:function(e){
    wx.request({
      url: app.globalData.urlBase + '/merchants',
      data:{
        name:encodeURI(e.detail.value)
      },
      success: res => {
        console.log(res.data);
        this.setData({ "brandList": res.data });
      }
    })
  },
  checkSearchItemName:function(){
    var searchItems = this.data.searchItems;
    for(var i in searchItems){
      if(searchItems[i]=='全部筛选'){
        if(i==0){
          searchItems[i]='距离最近';
        }else if(i==1){
          searchItems[i]='最新发布 ';
        }else if(i==2){
          searchItems[i]='浏览最多';
        }else if(i==3){
          searchItems[i]='评价最好';
        }
      }
    }
    this.setData({searchItems:searchItems});
  },

 

    //根据用户openid获得此用户发布的招聘信息
    getmessageByopenID:function(){
      const db= wx.cloud.database();
      db.collection("zhaopin").orderBy('sendtime','desc').where({_openid:app.globalData.openid}).get({
        success:res=>{
          console.log(res.data);
          that.setData({
            content:res.data
          })
        },
        fail:res=>{
          console.log("获取失败");
        },
      })
    },
    //获取数据库中所有的内容
    getallmessage:function(){
      const db=wx.cloud.database();
      db.collection("zhaopin").orderBy('sendtime','desc').get({
        success:res=>{
          console.log(res.data);
          that.setData({
            content:res.data
          })
        },
        fali:res=>{
          console.log("获取所有数据失败",res);
        }

      })
    }

})