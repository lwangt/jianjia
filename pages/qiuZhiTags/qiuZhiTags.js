
Page({
  /**
   * 页面的初始数据
   */
  data: {
    scrollheight:0,
    height: 0,
    indexActive: 0,
    // 产品分类大列表
    categorylist: [
      '技能','家教','服务','企业','其他'
    ],
    categoryChild: [],
    // 翻译
    tags1:[
      { message:'英语'  },
      { message:'西班牙语'  },
      { message:'闽南话'  },
    ],
    // 数字媒体
    tags2:[
      { message:'剪辑'  },
      { message:'修图'  },
      { message:'海报'  },
      { message:'调音'  }
    ],
    // PPT
    tags3:[
      { message:'答辩PPT'  },
      { message:'汇报PPT'  },
      { message:'代答辩'  }
    ],

  },


  /**
   * 生命周期函数--监听页面加载
   */
  List: [],
  onLoad() {
    // 页面加载时就计算scroll-view高度
    this.calScrollHeight() ;

    // 获取缓存中的数据  ，  默认值 是空字符串
    const list = wx.getStorageSync('list');
    if (!list) {
      //如果没有数据就发送请求
      this.getList()
    } else {
      // 如果有数据 再判断数据是否过期，根据时间
      if (Date.now() - list.time > 10 * 1000) {
        //数据过期了，重新获取数据
        this.getList()
      } else {
        // 没过期,将本地的数据渲染到视图中
        this.List = list.list;
        this.setData({
          categorylist: this.List.map(v => v.cat_name),
          categoryChild: this.List[this.data.indexActive].children
        })
      }
    }
  },
  
  //  发送异步请
  getList() {
    // 获取产品分类
    request({
        url: 'categories'
      })
      .then(res => {
        this.List = res.data.message,
          this.setData({
            categorylist: this.List.map(v => v.cat_name),
            categoryChild: this.List[this.data.indexActive].children
          })
        // 把数据存到缓存中
        wx.setStorageSync("list", {
          list: this.List,
          time: Date.now()
        });

      })

  },
  /* 右侧点击事件--改变样式 
   小程序传参藏在 e.currentTarget.dataset 里面
  */

  handleActive(e) {
    var index = e.currentTarget.dataset.index;
    this.setData({
      indexActive: index,
      categoryChild: this.List[index].children
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})