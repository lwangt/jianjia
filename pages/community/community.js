// pages/community/community.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userName: '小周波奇了',
    userJob: '青楼头牌·2年',
    question: '1办',
    likeNum: '233',
    commentNum: '233',
    Tabs:[
      {
        id:1,
        name:"推荐",
        url:"../../pages/recommend/recommend"
      },
      {
        id:2,
        name:"热帖",
        url:"../../pages/hot/hot"
      }
    ],
    sorts: ['拼车', '二手书', '其他', '其他1', '其他2'],

  },

  scroll(e) {
    console.log(e)
  },

    handleChange(e){
      // console.log(e);
      const{index}=e.detail;
      let {Tabs}=this.data;
      this.setData({
        Tabs
      })
    },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that =this;
     const db=wx.cloud.database();
     db.collection("luntan").orderBy('sendtime','desc').get({
       success:res=>{
         console.log("论坛数据获取成功")
         console.log(res.data)
         that.setData({
           content:res.data
         })
       },
       fali:res=>{
         console.log("获取所有数据失败",res);
       }
     })
  },

})