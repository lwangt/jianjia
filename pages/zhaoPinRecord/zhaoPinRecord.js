
var app=getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    
    show:-1,
    itemShow:[0,0,0,0],
    searchItems:['筛选'],
    selectItems:[
      ['全部筛选','距离最近','最新发布','浏览最多','评价最好'],
    ],
    tags:[
      { message:'每天两小时'  },
      { message:'每周至少三次'  },
      { message:'本科生'  }
    ],
    brandList:[
      { message:'1'  },
      { message:'2'  },
      { message:'3'  },
      { message:'4'  },
      { message:'5'  },
      { message:'6'  }
    ],
    score:4.2,
    pageIndex:1,
    name:'',
    jobname:'',
    yaoqiu:'',
    didian:'',
    time:'',
    money:'',
    email:'',
    sendtime:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that =this;
    const db=wx.cloud.database();
    
     //获取用户openid
    wx.cloud.callFunction({
      name: 'getopenID',
      data: {},
      success: res =>{
        app.globalData.openid =res.result.openid;
        console.log("11");
        console.log(app.globalData.openid);
      },
      fail: err =>{
        console.error("云函数调用失败", err);
      }, 

    })
    
      wx.request({
        url: app.globalData.urlBase +'/merchants',
        success:res=>{
          this.setData({"brandList":res.data});
        }
      })
      

       //根据用户openid获得此用户发布的招聘信息
       db.collection("zhaopin").orderBy('sendtime','desc').where({_openid:app.globalData.openid})
       .get({
        success:res=>{
          console.log("获取本人招聘信息成功");
          that.setData({
            content:res.data
          })
        },
        fail:res=>{
          console.log("获取失败");
        },
      })

      

  },

  onPullDownRefresh: function(){
    var that=this;
    that.setData({
      currentTab:0
    })
    this.onLoad();
    console.log("下拉刷新");
  },

    

    deleteThis:function(e){
      //从数据库删除本条记录
      const db=wx.cloud.database();
      console.log(e.currentTarget.dataset.idx)
      db.collection('zhaopin').doc(e.currentTarget.dataset.idx).remove({
        success: function(res) {
          console.log("招聘信息删除成功")
        }
      })
    }
})