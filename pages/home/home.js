// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userName: '点击图标登录',
    judge:0
  },

  handleGetUserInfo(e) {
    console.log(e);
    this.setData({
      userName: e.detail.userInfo.nickName,
      judge: 1
    })
  },

  goToQiuZhiRecord:function() {
    wx.navigateTo({
      url: "/pages/qiuZhiRecord/qiuZhiRecord",
    });
    console.log("==== go to qiuZhiRecord ");
  },
  goToZhaoPinRecord:function()  {
    wx.navigateTo({
      url: "/pages/zhaoPinRecord/zhaoPinRecord",
    });
    console.log("==== go to zhaoPinRecord ");
  },

})

