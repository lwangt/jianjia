// pages/news/news.js

Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:'',
    jobname:'',
    yaoqiu:'',
    didian:'',
    time:'',
    money:'',
    email:'',
    sendtime:'',
    tiaojian:''
  },
  
  inputname(e){
    //记得进行双向绑定！
    this.data.name =e.detail.value
    this.setData({
      name:this.data.name
    }) 
    },
  
    inputtime(e){
      //记得进行双向绑定！
      this.data.time =e.detail.value
      this.setData({
        time:this.data.time
      }) 
      },
      inputjobname(e){
        //记得进行双向绑定！
        this.data.jobname =e.detail.value
        this.setData({
          jobname:this.data.jobname
        }) 
      },
      inputyaoqiu(e){
        //记得进行双向绑定！
        this.data.yaoqiu =e.detail.value
        this.setData({
          yaoqiu:this.data.yaoqiu
        }) 
      },
      inputdidian(e){
        //记得进行双向绑定！
        this.data.didian =e.detail.value
        this.setData({
          didian:this.data.didian
        }) 
      },
      inputmoney(e){
        //记得进行双向绑定！
        this.data.money =e.detail.value
        this.setData({
          money:this.data.money
        }) 
      },
      inputemail(e){
        this.data.email =e.detail.value
        this.setData({
          email:this.data.email
        })
      },
      inputtiaojian(e){
        //记得进行双向绑定！
        this.data.tiaojian =e.detail.value
        this.setData({
          tiaojian:this.data.tiaojian
        }) 
      },
 

//点击提交，将数据存入数据库，并提示
inputLaber:function(){
  
  const db = wx.cloud.database();
  db.collection("qiuzhi").add({
    data: {
      
      name: this.data.name,
      jobname:this.data.jobname,
      yaoqiu:this.data.yaoqiu,
      didian:this.data.didian,
      time:this.data.time,
      money:this.data.money,
      email:this.data.email,
      tiaojian:this.data.tiaojian,
      sendtime: new Date().getTime()
    },
    success: res=>{
      console.log("求职信息至数据库成功")
      wx.showToast({
        title: "发布信息成功"
      });
    },
    fail: err=>{
      console.error("失败：", err);
    }
  });

  //传数据结束跳转回去
  var pages = getCurrentPages();
  var before = pages[pages.length - 2];
  wx.switchTab({
    url: "/pages/qiuZhi/qiuZhi",
  });
  before.onLoad();
  console.log("==== go to qiuZhi ");

}
  
})