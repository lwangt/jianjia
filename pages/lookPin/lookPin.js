// pages/lookPin/lookPin.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    score:4.2,
    brandList:[
      { message:'1'  },
      { message:'2'  },
      { message:'3'  },
      { message:'4'  },
      { message:'5'  },
      { message:'6'  },
      { message:'7'  },
      { message:'8'  },
      { message:'9'  },
      { message:'10'  },
      { message:'11'  },
      { message:'12'  },
      { message:'13'  },
      { message:'14'  },
      { message:'15'  },
      { message:'16'  },
      { message:'17'  },
      { message:'18'  },
      { message:'19'  },
      { message:'20'  }
    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})