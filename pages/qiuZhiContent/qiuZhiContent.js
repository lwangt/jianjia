
var app=getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    
    show:-1,
    itemShow:[0,0,0,0],
    searchItems:['筛选'],
    selectItems:[
      ['全部筛选','距离最近','最新发布','浏览最多','评价最好'],
    ],
    tags:[
      { message:'每天两小时'  },
      { message:'每周至少三次'  },
      { message:'本科生'  }
    ],
    brandList:[
      { message:'1'  },
      { message:'2'  },
      { message:'3'  },
      { message:'4'  },
      { message:'5'  },
      { message:'6'  }
    ],
    score:4.2,
    pageIndex:1,
    name:'',
    jobname:'',
    yaoqiu:'',
    didian:'',
    time:'',
    money:'',
    email:'',
    sendtime:'',
    tiaojian:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    options.idx
    let that =this;
    const db=wx.cloud.database();
    
    

       //根据_id选择数据
       db.collection("qiuzhi").where({
         _id:options.idx
       }).get({
        success:res=>{
          that.setData({
            content:res.data
          })
        },
        fail:res=>{
          console.log("获取失败");
        },
      })


  },

  onPullDownRefresh: function(){
    var that=this;
    that.setData({
      currentTab:0
    })
    this.onLoad();
    console.log("下拉刷新");
  },

   
  

})