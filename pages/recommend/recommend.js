// pages/recommend/recommend.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userName: '小周波奇了',
    userJob: '青楼头牌·2年',
    question: '软工实践ForeverGod',
    likeNum: '233',
    commentNum: '233',
    Tabs:[
    
      {
        id:1,
        name:"关注",
        url:"../../page/community/community"
      },
      {
        id:2,
        name:"热帖",
        url:"../../pages/hot/hot"
      }
    ]
  },
  handleChange(e){
    // console.log(e);
    const{index}=e.detail;
    let {Tabs}=this.data;
    this.setData({
      Tabs
    })
  },
  handleNavTo(e) {
    console.log(e);
  },

})