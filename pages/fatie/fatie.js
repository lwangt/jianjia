// pages/news/news.js
var app=getApp();


Page({

  
  /**
   * 页面的初始数据
   */
  data: {
    name:'',
    text:'',
    sendtime:'',
    tags: ['拼车', '二手书', '其他', '其他1', '其他2'],
    tagIdx: 0,
  },
  
  bindPickerChange: function(e) {
    this.setData({
      tagIdx: e.detail.value
    })
  },

  inputname(e){
    //记得进行双向绑定！
    this.data.name =e.detail.value
    this.setData({
      name:this.data.name
    }) 
    },

  inputtext(e){
    //记得进行双向绑定！
    this.data.text =e.detail.value
    this.setData({
      text:this.data.text
    }) 
  },
  
  
  //点击提交，将数据存入数据库，并提示
  inputLaber:function(){

  const db = wx.cloud.database();
  db.collection("luntan").add({
    data: {   
      name: this.data.name,
      text: this.data.text,
      sort: this.data.tags[this.data.tagIdx],
      sendtime: new Date().getTime()
    },
    success: res=>{
      console.log("发布信息至数据库成功")
      wx.showToast({
        title: "发布信息成功"
      });
    },
    fail: err=>{
      console.error("失败：", err);
    }
  });

  var pages = getCurrentPages();
  var before = pages[pages.length - 2];

  //传数据结束跳转回去
  wx.switchTab({
    url: "/pages/community/community",
  });
  before.onLoad();
  console.log("==== go to community ");




}
  
})