// commpents/tabs/tabs.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    Tabs:{
      type:Array,
      value:[]
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    Tabs:[
      {
        id:0,
        name:"关于",
        isActive:false,
        url:"../../pages/community/community"
      },
      {
        id:1,
        name:"推荐",
        isActive:false,
        url:"../../pages/recommend/recommend"
      },
      {
        id:2,
        name:"热帖",
        isActive:false,
        url:"../../pages/hot/hot"
      }
    ]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    handletap(e){
      const {index}=e.currentTarget.dataset;
      this.triggerEvent("itemChange",{index});

      // let {tabs}=this.data;
      // tabs.forEach((v,i)=>i===index?v.isActive=true:v.isActive=false);  
      // this.setData({tabs})

    }
  }
})
